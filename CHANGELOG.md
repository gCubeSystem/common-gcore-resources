# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0]

- Moved to jakarta 


## [v1.3.5] - 2023-03-30

- See  #24727 upgrade version just for removing the gcube-staging dependencies 
- Add changelog, readme, license
